import {AsyncStorage} from 'react-native';

const loginReducer = (state = {status: false}, action) => {

    if (action.type === 'LOGIN_SUCCESS') {
        //save token here
        let token = action.jwtToken;
        AsyncStorage.setItem('jwtToken', token);

        // getUserInfo().then((response) => {
        //     AsyncStorage.setItem('userInfo', JSON.stringify(response.data));
        // }).catch(err => {
        //     console.log(err);
        // });

        return {
            message: 'Đăng nhập thành công',
            status: true
        }
    }
    if (action.type === 'WRONG_USERNAME_PASSWORD') {
        return {
            message: 'Sai tên đăng nhập/mật khẩu',
            status: false
        }
    }
    if (action.type === 'START_LOGIN') {
        return {
            message: 'Đang đăng nhập...',
            status: false
        }
    }
    if (action.type === 'LOGOUT') {
        return {
            message: '',
            status: false
        }
    }

    if (action.type === 'LOGIN_ERROR') {
        return {
            message: 'Lỗi đăng nhập',
            status: false
        }
    }

    if (action.type === 'NETWORK_ERROR') {
        return {
            message: 'Không có kết nối internet. Vui lòng thử lại!',
            status: false
        }
    }

    if (action.type === 'OUT_OF_DATE') {
        return {
            message: 'Tài khoản TUHA không hợp lệ hoặc đã hết hạn sử dụng',
            status: false
        }
    }

    return state;
};

export default loginReducer;
