import { combineReducers } from 'redux';
import loginReducer from './loginReducer';

const reducer = combineReducers({
    loginResult: loginReducer,
});

export default reducer;
