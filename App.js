import React from 'react';
import AppNavigator from './navigation/AppNavigator';
import {Platform, StatusBar} from 'react-native';
import {AppLoading} from "expo";
import * as Font from 'expo-font'
import {Root} from "native-base";
import {Provider} from 'react-redux';
import store from './stores/store';

class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
    };
  }

  async componentDidMount() {
    await Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
      Ionicons: require("native-base/Fonts/Ionicons.ttf"),
    });
    this.setState({
      loading: false
    });
  }

  render() {
    if (this.state.loading) {
      return (
          <Root>
            <AppLoading/>
          </Root>
      );
    }
    return (
        <Provider store={store}>
          <Root>
            <StatusBar backgroundColor="#2a81ab" barStyle="light-content"/>
            <AppNavigator/>
          </Root>
        </Provider>

    );
  }
}

export default App;

