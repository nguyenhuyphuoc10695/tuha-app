import React, {Component} from 'react';
import {connect} from 'react-redux';
import {login, loginWithTuha} from '../actions/actionLogin';
import Dimension from "../constants/Dimension";
import {
    StyleSheet,
    View,
    AsyncStorage,
    KeyboardAvoidingView,
    ScrollView,
    StatusBar,
    Platform,
    ImageBackground
} from "react-native";
import {Item, Input, Text, Icon, Thumbnail, Button} from 'native-base';
import Colors from "../constants/Colors";
// import {Header, useHeaderHeight } from 'react-navigation-stack';

const SCREEN_WIDTH = Dimension.window.width;
const SCREEN_HEIGHT = Dimension.window.height;

class LoginScreen extends Component {
    static navigationOptions = {
        headerShown: false
    };

    constructor(props) {
        super(props);
        AsyncStorage.clear();
        this.state = {
            username: '',
            password: '',
            message: '',
            // showError: false
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.status) {
            this.props.navigation.navigate('AuthLoading');
        }
        return false;
    }

    getUserName = (text) => {
        this.setState({username: text});
    };

    getPassword = (text) => {
        this.setState({password: text});
    };

    render() {
        // const {navigate} = this.props.navigation;
        // const headerHeight = Header.HEIGHT;
        const statusBarHeight = StatusBar.currentHeight;
        const heightToPush = Platform.OS === 'ios' ? 64 : (100 + statusBarHeight);
        return (
            <ScrollView keyboardDismissMode="on-drag"
                        keyboardShouldPersistTaps="always">
                <KeyboardAvoidingView behavior="padding" keyboardVerticalOffset={heightToPush}
                                      enabled>
                    <ImageBackground source={require("../assets/tuha_background.png")} style={styles.backgroundImage}>
                        <View style={styles.container}>
                            <View style={styles.content}>
                                <Text style={styles.btnMessage}>{this.props.message}</Text>

                                <Item style={styles.item}>
                                    <Icon ios='ios-person' android="md-person"/>
                                    <Input placeholder='Tên đăng nhập'
                                           onChangeText={this.getUserName}
                                           value={this.state.username}
                                    />
                                </Item>
                                <Item style={styles.item}>
                                    <Icon ios='ios-unlock' android="md-unlock"/>
                                    <Input placeholder='Mật khẩu'
                                           secureTextEntry={true}
                                           onChangeText={this.getPassword}
                                           value={this.state.password}
                                    />
                                </Item>
                                <View style={styles.btnLogin}>
                                    <Button full block info
                                            onPress={() => this.props.login({
                                                username: this.state.username,
                                                password: this.state.password
                                            })}
                                    >
                                        <Text>ĐĂNG NHẬP</Text>
                                    </Button>
                                </View>
                                {/*<View style={styles.btnLogin}>*/}
                                    {/*<Button full block info*/}
                                            {/*onPress={() => this.props.loginWithTuha({*/}
                                                {/*username: this.state.username,*/}
                                                {/*password: this.state.password*/}
                                            {/*})}*/}
                                    {/*>*/}
                                        {/*<Text>Đăng nhập qua TUHA</Text>*/}
                                    {/*</Button>*/}
                                {/*</View>*/}
                            </View>
                        </View>
                    </ImageBackground>

                </KeyboardAvoidingView>
            </ScrollView>

        );
    }
}

function mapStateToProps(state) {
    return {
        username: state.username,
        password: state.password,
        message: state.loginResult.message,
        status: state.loginResult.status
    };
}

export default connect(mapStateToProps, {login, loginWithTuha})(LoginScreen);

const styles = StyleSheet.create({
    backgroundImage: {
        flex: 1,
        resizeMode: "cover",
        justifyContent: "center"
    },
    signin: {
        position: 'absolute',
        bottom: 10,
        width: (SCREEN_WIDTH * 7) / 8,
        justifyContent: 'center',
        alignItems: 'center',
    },
    btnLogin: {
        marginTop: 30,
    },
    bigTitle: {
        color: '#ffffff',
        fontWeight: 'bold',
        fontSize: 25,
        textAlign: 'center',
        paddingBottom: 20,

    },
    logo: {
        flexDirection: 'row',
        justifyContent: 'center',
        paddingTop: 30,
        // alignItems: 'center',
    },
    logoText: {
        paddingLeft: 10,
    },
    logoImage: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    txtError: {
        color: 'red',
        fontSize: 14,
        textAlign: 'center',
        paddingTop: 15,
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        height: SCREEN_HEIGHT,
        width: SCREEN_WIDTH,
        // backgroundColor: Colors.tintColor,
    },
    content: {
        // backgroundColor: '#ffffff',
        width: (SCREEN_WIDTH * 7) / 8,
        padding: 15,
        overflow: 'hidden',
        height: SCREEN_HEIGHT/ 2,
        // borderRadius: 10,
        // marginTop: SCREEN_HEIGHT/4,
    },
    item: {
        marginTop: 20,
    },
    btnItem: {
        marginTop: 20,
        borderBottomWidth: 0,
    },
    button: {
        width: (SCREEN_WIDTH * 7) / 8,
        backgroundColor: '#20a8d8',
        paddingVertical: 15,
        // borderRadius: 5,
    },
    btnText: {
        color: '#fff',
        marginLeft: 5,
        fontSize: 24,
    },
    loginIcon: {
        color: '#fff',
    },
    icon: {
        // textAlign: 'center',
    },
    btnContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    title: {
        color: '#000000',
        fontWeight: 'bold',
        fontSize: 24,
        textAlign: 'center',

    },
    btnMessage: {
        marginTop: 20,
        textAlign: 'center',
        color: '#d9534f'
    }
});
