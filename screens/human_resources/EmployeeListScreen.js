import React, {Component} from 'react';
import {
    Button,
    Text,
    Container,
    Body,
    Content,
    Header,
    Title,
    Left,
    Icon,
    Drawer,
    Right, ListItem, Thumbnail, List
} from "native-base";
import {Platform, StatusBar, StyleSheet, View, ScrollView, Image} from 'react-native'
import SideBar from "../../components/SideBar";
import Dimension from "../../constants/Dimension";
import Colors from "../../constants/Colors";
import _ from 'lodash';

const SCREEN_WIDTH = Dimension.window.width;
const SCREEN_HEIGHT = Dimension.window.height;

export default class EmployeeListScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    closeDrawer() {
        this.drawer._root.close()
    }

    openDrawer() {
        this.drawer._root.open()
    };

    render() {
        return(
            <View></View>
        )
    }
}

const styles = StyleSheet.create({
    cupImage: {
        width: 20,
        height: 20,
    },
    txtName: {
        fontSize: 20,
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    col_left: {
        width: (SCREEN_WIDTH - 20) * 0.5,
        borderBottomColor: 'lightgrey',
        borderBottomWidth: 1,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10,
        borderRightColor: 'lightgrey',
        borderRightWidth: 0.5,
    },
    col_right: {
        width: (SCREEN_WIDTH - 20) * 0.5,
        borderBottomColor: 'lightgrey',
        borderBottomWidth: 1,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10,
        borderLeftColor: 'lightgrey',
        borderLeftWidth: 0.5,
    },
    icon: {
        borderColor: 'lightgrey',
        backgroundColor: '#f7f7f7',
        width: (SCREEN_WIDTH - 20) * 0.25,
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 20,
        overflow: 'hidden',
        borderRadius: (SCREEN_WIDTH - 20) * 0.125,
    },
    text: {
        textTransform: 'uppercase',
        textAlign: 'center',
        color: Colors.tintColor,
        // fontSize: 14,
        paddingVertical: 17,
    },
    container: {
        ...Platform.select({
            android: {
                paddingTop: StatusBar.currentHeight
            }
        }),
        backgroundColor: Colors.tintColor,
    },
    content: {
        backgroundColor: '#ffffff'
    },
    body: {
        flex: 2,
        ...Platform.select({
            ios: {
                flex: 2,
            }
        }),
    },
    txtBold: {
        fontWeight: 'bold',
    },
    item: {
        margin: 10,
    },
});
