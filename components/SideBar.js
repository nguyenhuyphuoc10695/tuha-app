import React from "react";
import {AsyncStorage, Image, StyleSheet, View, Alert} from "react-native";
import {
    Text,
    Container,
    List,
    ListItem,
    Content,
    Icon,
    Left,
    Button,
    Body, Thumbnail,
} from "native-base";
import {connect} from "react-redux";
import {logout} from "../actions/actionLogin";
import Colors from "../constants/Colors";
import {isAdminRole} from "../apis/authenication";

export class SideBar extends React.Component{

    constructor(props) {
        super(props);
        this.state = {

        }
    }

    logout = () => {
        Alert.alert(
            'Đăng xuất',
            'Bạn có muốn đăng xuất không?',
            [
                {
                    text: 'Đồng ý', onPress: () => {
                        AsyncStorage.clear().then(result => {
                            this.props.logout();
                            this.props.navigation.navigate('AuthLoading');
                        })
                    }
                },
                {
                    text: 'Hủy',
                    onPress: () => console.log('Hủy'),
                    style: 'cancel',
                }
            ],
            {
                cancelable: true
            },
        );

    };

    render() {
        return (
            <Container>
                <Content>
                    <View style={styles.logo}>
                        <Image style={styles.image} source={require("../assets/tuha-logo-app.png")}/>
                    </View>

                    <ListItem
                        icon
                        button
                        onPress={() => this.props.navigation.navigate('Report')}
                    >
                        <Left>
                            <Icon ios='ios-stats' android="md-stats" style={styles.icon}/>
                        </Left>
                        <Body>
                        <Text style={styles.label}>Danh sách báo cáo</Text>
                        </Body>
                    </ListItem>

                    <ListItem
                        icon
                        button
                        onPress={() => this.props.navigation.navigate('Report')}
                    >
                        <Left>
                            <Icon ios='ios-cart' android="md-cart" style={styles.icon}/>
                        </Left>
                        <Body>
                        <Text style={styles.label}>Quản lý đơn hàng</Text>
                        </Body>
                    </ListItem>

                    <ListItem
                        icon
                        button
                        onPress={() => this.props.navigation.navigate('HumanResource')}
                    >
                        <Left>
                            <Icon ios='ios-people' android="md-people" style={styles.icon}/>
                        </Left>
                        <Body>
                        <Text style={styles.label}>Quản lý nhân viên</Text>
                        </Body>
                    </ListItem>

                    <ListItem
                        icon
                        button
                        onPress={() => this.logout()}
                    >
                        <Left>
                            <Icon ios='ios-log-out' android="md-log-out" style={styles.iconLogOut}/>
                        </Left>
                        <Body>
                        <Text style={styles.label}>Đăng xuất</Text>
                        </Body>
                    </ListItem>
                </Content>
            </Container>
        );
    }
}

function mapStateToProps(state) {
    return {
        // userInfoData: state.userInfoResult.userInformation,
        // domain: state.domainResult.domainInfo,
    };
}

export default connect(mapStateToProps, {logout})(SideBar);

const styles = StyleSheet.create({
    iconLogOut: {
        color: '#47b5e0',
    },
    logo: {
        flexDirection: 'row',
        justifyContent: 'center',
        paddingVertical: 17,
        backgroundColor: '#f7f7f7',
        alignItems: 'center',
    },
    logoText: {
        paddingLeft: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    logoImage: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    image: {
        width: 300,
        height: 95,
    },
    title: {
        fontWeight: 'bold',
        fontSize: 37,
        textAlign: 'center',
        color: '#47b5e0',
    },
    icon: {
        color: '#47b5e0',
    },
    label: {
        color: '#47b5e0',
    },
});
