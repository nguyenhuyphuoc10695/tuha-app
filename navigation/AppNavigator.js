import React from 'react';
import {createSwitchNavigator, createAppContainer,} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import AuthLoadingScreen from "../screens/AuthLoadingScreen";
import LoginScreen from "../screens/LoginScreen";
import ReportListScreen from "../screens/reports/ReportListScreen";
import EmployeeListScreen from "../screens/human_resources/EmployeeListScreen";

const AuthStack = createStackNavigator(
    {
        LogIn: {
            screen: LoginScreen,
            navigationOptions: {
                headerShown: false,
            }
        },
    }
);

const ReportStack = createStackNavigator(
    {
        Report: {
            screen: ReportListScreen,
            navigationOptions: {
                headerShown: false,
            }
        },
    }
);

const HumanResourceStack = createStackNavigator(
    {
        HumanResource: {
            screen: EmployeeListScreen,
            navigationOptions: {
                headerShown: false,
            }
        },
    }
);

export default createAppContainer(createSwitchNavigator(
    {
        AuthLoading: AuthLoadingScreen,
        Auth: AuthStack,
        ReportList: ReportStack,
        HumanResourceList: HumanResourceStack,
    },
    {
        initialRouteName: 'AuthLoading'
    }
));
